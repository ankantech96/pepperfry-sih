
var getMyIP = require('get-my-ip')

var bodyParser = require('body-parser');
var express = require('express');
var multer = require('multer');
var mongoose = require('mongoose');
const port=process.env.PORT || 1000;
 
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/Pepperfry', { useNewUrlParser: true });

var app = express();
var mkdirp = require('mkdirp');

app.use(bodyParser.json());

var armodels = new mongoose.Schema({
  days:Number,
  modelname:String,
  price:Number,
  isarenabled:Boolean,
  maxscale:Number,
  sfa:String,
  sfb:String,
  png:String,
  txt:String,
  obj:String,
  mtl:String,
  jpeg:String,
  jpg:String,
  type:String,
  ispepperfry:Boolean


}, { strict: false });
var logs=new mongoose.Schema({
  name:String,
  mid:String,
  timestamp:Number,
  activity:String,
  event:String,

})


var Model = mongoose.model('Model', armodels);
var Logmodel =mongoose.model('Logmodel',logs);


var storage = multer.diskStorage({
  destination: function (req, file, cb) {

    mkdirp('./uploads/' + req.params.id, function (err) {

      if (err)
        console.log(err);
      else
        cb(null, './uploads/' + req.params.id);

    })

  },
  filename: function (req, file, cb) {
    console.log(file);
    cb(null, file.originalname) 
  }
})




app.get('/all', (req, res) => {
  Model.find().then((doc) => {
    res.status(200).send(doc);
  }).catch((err)=>{
    res.status(500).send(err);
    })
})
app.get('/getmodel/:id', (req, res) => {
  console.log(req.params.id.toString());
  
  Model.findById(req.params.id.toString()).then((doc) => {
    res.status(200).send(doc);
  }).catch((err)=>{res.status(500).send(err)})
})
var upload = multer({ storage: storage });
app.post('/fileupload/:id', upload.array('avatar', 5), function (req, res, next) {

  if (!req.files) {
    console.log("No file received");
    return res.status(500).send({
      success: false
    });

  } else {

    var obj = {}
    var modeldetails = req.files;
    modeldetails.forEach((element) => {
     
      var t = element.originalname.split('.');
      var x = t[t.length - 1].toLowerCase();

      obj[`${x}`] = "/uploads/" + req.params.id + '/' + element.originalname;

    })
    obj["modelname"] = req.params.id;
    obj["price"] = req.body.price;
    obj["isarenabled"] = req.body.isarenabled;
    obj["days"] = req.body.days;
    obj["maxscale"] = req.body.maxscale;
    const au = new Model(obj);
    au.save().then((doc) => {
      res.status(200).send(doc);
    })


  }
})
app.post('/fileupdate/:id', upload.array('avatar', 1), function (req, res, next) {
  var oid=req.body.oid;
  console.log(oid);

  if (!req.files) {
    console.log("No file received");
    return res.status(500).send({
      success: false
    });

  } else {
    Model.findById(req.body.oid.toString()).then((doc) => {
      var modeldetails = req.files;
      modeldetails.forEach((element) => {
        //  console.log(element.filename);
        var t = element.originalname.split('.');
        var x = t[t.length - 1].toLowerCase();
  
        doc[`${x}`] = "/uploads/" + req.params.id + '/' + element.originalname;
  
      })
     
      doc.save().then((doc) => {
        res.status(200).send(doc);
      })
  
        }).catch((err)=>{res.status(500).send(err)})
    

   
    


  }
})
var path = require('path');

app.get('/uploads/:id/:i', (req, res) => {
  console.log(req.params.id, req.params.i);
  var path = require('path');
  var fs = require('fs');
  var file = path.join(__dirname, './uploads/' + req.params.id + '/' + req.params.i);
  console.log("file is ",file);
  var data = fs.readFileSync(file);
  res.send(data);

})
app.get('/test',(req,res)=>{
res.send("hello world");
})
app.get('/getrecommendations/:id',(req,res)=>{
  Model.find({"type":req.params.id}).then((doc)=>{
    res.status(200).send(doc);
  }).catch((err)=>{
    res.status(500).send(err)
  })

})
app.post('/logs',(req,res)=>{
  var newlog= new Logmodel(req.body);
  newlog.save().then((doc)=>{
    res.status(200).send(doc);
  }).catch((err)=>{
    res.status(500).send(err);

  });
  app.get('/alllogs',(req,res)=>{
Logmodel.find().then((doc)=>{
  res.status(200).send(doc);
}).catch((err)=>{
  res.status(500).send(err);
})
  })

})
app.listen(port, (req, res) => {
  console.log("Server is running")
})